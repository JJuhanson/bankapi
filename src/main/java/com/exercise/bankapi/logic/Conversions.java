package com.exercise.bankapi.logic;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Conversions {

    private static final Logger LOGGER = LoggerFactory.getLogger(Conversions.class);

    private static final int PRECISION = 2;


    public static BigDecimal convertToCurrency(BigDecimal amount, Currency current, Currency toBe) {
        if (current.equals(toBe)) {
            return amount;
        }
        return (amount.multiply(toBe.getRate())).divide(current.getRate(), PRECISION, RoundingMode.HALF_EVEN);
    }

    public static BigDecimal convertStringToDecimal(String string) {
        if (string == null) {
            return null;
        }
        try {
            return new BigDecimal(string).setScale(PRECISION, RoundingMode.HALF_EVEN);
        } catch (NumberFormatException e) {
            LOGGER.warn("Provided string {} is not number", string);
            return null;
        }
    }


}

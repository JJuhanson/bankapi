package com.exercise.bankapi.logic;

import java.math.BigDecimal;
import java.util.Arrays;

public enum Currency {
    EUR("1"), USD("1.12560"), SEK("10.25730"), RUB("83.7755");

    private final BigDecimal rate;

    Currency(String rate) {
        this.rate = new BigDecimal(rate);
    }

    public BigDecimal getRate() {
        return rate;
    }

    /**
     * Returns null if currency does not exist
     */
    public static Currency getCurrency(String currencyString) {
        if (currencyString == null) return null;
        return Arrays.stream(values()).filter(currency -> currency.name().equals(currencyString.toUpperCase())).findFirst().orElse(null);
    }
}

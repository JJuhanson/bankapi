package com.exercise.bankapi.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorResult {

    @JsonProperty
    private String message;

    public ErrorResult(String message) {
        this.message = message;
    }
}

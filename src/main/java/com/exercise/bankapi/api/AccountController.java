package com.exercise.bankapi.api;

import com.exercise.bankapi.database.entity.Balance;
import com.exercise.bankapi.exceptions.ClientException;
import com.exercise.bankapi.exceptions.ServerException;
import com.exercise.bankapi.services.AccountControllerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("accounts")
@Tag(name = "Account", description = "the Account API")

public class AccountController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountControllerService service;

    @Operation(summary = "Finds all user balances")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Returns all user balances"),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(schema = @Schema(implementation = ErrorResult.class)))})
    @GetMapping(value = "/{user}", produces = "application/json")
    public List<Balance> getBalances(@PathVariable String user) {
        return service.getBalances(user);
    }

    @Operation(summary = "Adds money to balance", description = "Adds money to specified currency balance. If balance is missing adds new balance with specified currency")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "400", description = "Provided currency or amount was invalid", content = @Content(schema = @Schema(implementation = ErrorResult.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(schema = @Schema(implementation = ErrorResult.class)))})
    @PostMapping(value = "/{user}/add", produces = "application/json")
    public Balance addMoney(@PathVariable String user, @RequestBody @io.swagger.v3.oas.annotations.parameters.RequestBody RequestDTO request) {
        return service.addMoney(user, request.getCurrency(), request.getAmount());
    }

    @Operation(summary = "Debit money from account balance")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "400", description = "Provided currency or amount was invalid or user does not have sufficient funds", content = @Content(schema = @Schema(implementation = ErrorResult.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(schema = @Schema(implementation = ErrorResult.class)))})
    @PostMapping(value = "/{user}/debit", produces = "application/json")
    public Balance debitMoney(@PathVariable String user, @RequestBody @io.swagger.v3.oas.annotations.parameters.RequestBody RequestDTO request) {
        return service.debitMoney(user, request.getCurrency(), request.getAmount());
    }

    @Operation(summary = "Exchange money from one currency to another")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Successful operation"),
            @ApiResponse(responseCode = "400", description = "Provided currencies or amount was invalid or user does not have sufficient funds", content = @Content(schema = @Schema(implementation = ErrorResult.class))),
            @ApiResponse(responseCode = "500", description = "Internal server error", content = @Content(schema = @Schema(implementation = ErrorResult.class)))})
    @PostMapping(value = "/{user}/exchange", produces = "application/json")
    public List<Balance> currencyExchange(@PathVariable String user, @RequestBody @io.swagger.v3.oas.annotations.parameters.RequestBody RequestExchangeDTO request) {
        return service.exchangeMoney(user, request.getFromCurrency(), request.getToCurrency(), request.getAmount());
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ClientException.class)
    public ErrorResult handleClientError(ClientException ex) {
        return new ErrorResult(ex.getMessage());
    }

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ServerException.class)
    public ErrorResult handleServerError(ServerException ex) {
        LOGGER.error("Received internal error", ex);
        return new ErrorResult("Please contact administrator");
    }
}

package com.exercise.bankapi.database;

import com.exercise.bankapi.database.entity.Balance;
import org.springframework.data.repository.CrudRepository;

public interface BalanceRepository extends CrudRepository<Balance, Long> {
}

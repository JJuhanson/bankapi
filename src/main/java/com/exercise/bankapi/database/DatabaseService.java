package com.exercise.bankapi.database;

import com.exercise.bankapi.database.entity.Balance;
import com.exercise.bankapi.exceptions.ServerException;
import com.exercise.bankapi.logic.Currency;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class DatabaseService {

    @Autowired
    private SessionFactory sessionFactory;


    private List<Balance> makeDatabaseRead(Map<String, String> parameters) {
        try (Session session = sessionFactory.openSession()) {
            CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();
            CriteriaQuery<Balance> query = criteriaBuilder.createQuery(Balance.class);
            Root<Balance> from = query.from(Balance.class);
            List<Predicate> predicates = parameters.entrySet().stream().map(entry ->
                    criteriaBuilder.equal(from.get(entry.getKey()), entry.getValue())).collect(Collectors.toList());
            query.where(predicates.toArray(new Predicate[]{}));

            return session.createQuery(query).getResultList();
        } catch (Exception e) {
            throw new ServerException(e);
        }
    }

    public void updateDatabase(Balance balance) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.getTransaction();
            transaction.begin();
            session.saveOrUpdate(balance);
            transaction.commit();
        } catch (Exception e) {
            throw new ServerException(e);
        }
    }

    public Balance getAccountBalance(String user, Currency currency) {
        Map<String, String> parameters = Map.of("user", user, "currency", currency.name());
        return makeDatabaseRead(parameters).stream().findFirst().orElse(null);
    }

    public List<Balance> getAccountBalances(String user) {
        return makeDatabaseRead(Collections.singletonMap("user", user));
    }


}

package com.exercise.bankapi.services;

import com.exercise.bankapi.database.DatabaseService;
import com.exercise.bankapi.database.entity.Balance;
import com.exercise.bankapi.exceptions.ClientException;
import com.exercise.bankapi.exceptions.ServerException;
import com.exercise.bankapi.logic.Conversions;
import com.exercise.bankapi.logic.Currency;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Consumer;

@Service
public class AccountControllerService {

    @Autowired
    private DatabaseService databaseService;

    @Autowired
    private ExternalAsyncService externalAsyncService;

    private BigDecimal convertAmount(String amount) {
        BigDecimal amountDecimal = Conversions.convertStringToDecimal(amount);
        if (amountDecimal == null || amountDecimal.compareTo(BigDecimal.ZERO) <= 0) {
            throw new ClientException("Provided amount is not correct");
        }
        return amountDecimal;
    }

    private Currency convertCurrency(String currencyString) {
        Currency currency = Currency.getCurrency(currencyString);
        if (currency == null) {
            throw new ClientException("Provided currency does not exist");
        }
        return currency;
    }

    private Balance removeMoney(String user, Currency currency, BigDecimal amountToRemove, Consumer<Balance> callback) {
        Balance balance = databaseService.getAccountBalance(user, currency);
        if (balance == null) {
            throw new ClientException("User does not have requested currency funds");
        }
        BigDecimal currentAmount = balance.getAmount();
        if (amountToRemove.compareTo(currentAmount) > 0) {
            throw new ClientException("User has insufficient funds");
        }
        if (callback != null) {
            callback.accept(balance);
        }
        balance.setAmount(balance.getAmount().subtract(amountToRemove));
        databaseService.updateDatabase(balance);
        return balance;
    }

    private Balance addMoney(String user, Currency currency, BigDecimal amount) {
        Balance balance = databaseService.getAccountBalance(user, currency);
        if (balance == null) {
            balance = new Balance(user, amount, currency);
        } else {
            balance.setAmount(balance.getAmount().add(amount));
        }
        databaseService.updateDatabase(balance);
        return balance;
    }

    public List<Balance> getBalances(String user) {
        return databaseService.getAccountBalances(user);
    }


    public Balance addMoney(String user, String currencyString, String amountString) {
        BigDecimal amount = convertAmount(amountString);
        Currency currency = convertCurrency(currencyString);
        return addMoney(user, currency, amount);
    }


    public Balance debitMoney(String user, String currencyString, String amountString) {
        BigDecimal amount = convertAmount(amountString);
        Currency currency = convertCurrency(currencyString);
        return removeMoney(user, currency, amount, balance -> externalAsyncService.call());
    }


    public List<Balance> exchangeMoney(String user, String fromCurrencyString, String toCurrencyString, String amountString) {
        BigDecimal amount = convertAmount(amountString);
        Currency fromCurrency = convertCurrency(fromCurrencyString);
        Currency toCurrency = convertCurrency(toCurrencyString);

        BigDecimal toCurrencyAmount = Conversions.convertToCurrency(amount, fromCurrency, toCurrency);

        removeMoney(user, fromCurrency, amount, null);
        try {
            addMoney(user, toCurrency, toCurrencyAmount);
        } catch (ServerException e) {
            addMoney(user, fromCurrency, amount); //Trying to rollback remove of old currency money
            throw new ServerException(e);
        }
        return getBalances(user);
    }
}

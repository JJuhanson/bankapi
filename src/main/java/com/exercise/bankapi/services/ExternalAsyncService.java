package com.exercise.bankapi.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;


/**
 * Decided to use async request because task example was - external logging.
 * Chose async because it would make controller response speed faster.
 * If this call needs to effect the logic of controller then I would have chosen it to be sync request. For example, some
 * smartId confirmation.
 */
@Service
public class ExternalAsyncService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExternalAsyncService.class);

    public void call() {
        LOGGER.info("Making call to external async service");
        WebClient.create().get()
                .uri("https://httpstat.us/200?sleep=5000")
                .exchangeToMono(clientResponse -> clientResponse.statusCode().is2xxSuccessful() ?
                        Mono.just("Call to external async service successful (2xx)") :
                        Mono.just("Call to external async service not successful (not 2xx)"))
                .subscribe(value -> LOGGER.info("External async service call ended with message {}", value));
    }
}

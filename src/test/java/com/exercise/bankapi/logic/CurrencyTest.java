package com.exercise.bankapi.logic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CurrencyTest {

    @Test
    void getCurrency() {
        Assertions.assertEquals(Currency.EUR, Currency.getCurrency("EUR"));
        Assertions.assertEquals(Currency.RUB, Currency.getCurrency("RUB"));
        Assertions.assertEquals(Currency.SEK, Currency.getCurrency("SEK"));
        Assertions.assertEquals(Currency.USD, Currency.getCurrency("USD"));

        Assertions.assertEquals(Currency.EUR, Currency.getCurrency("eur"));
        Assertions.assertEquals(Currency.RUB, Currency.getCurrency("rub"));
        Assertions.assertEquals(Currency.SEK, Currency.getCurrency("sek"));
        Assertions.assertEquals(Currency.USD, Currency.getCurrency("usd"));

        Assertions.assertNull(Currency.getCurrency(null));
        Assertions.assertNull(Currency.getCurrency("null"));
        Assertions.assertNull(Currency.getCurrency(" "));
        Assertions.assertNull(Currency.getCurrency(""));
        Assertions.assertNull(Currency.getCurrency("GBP"));
    }
}
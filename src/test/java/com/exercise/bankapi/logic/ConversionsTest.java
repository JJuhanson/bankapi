package com.exercise.bankapi.logic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

class ConversionsTest {

    @Test
    void convertToCurrency() {
        Assertions.assertEquals(new BigDecimal("10.1"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.EUR, Currency.EUR));
        Assertions.assertEquals(new BigDecimal("11.37"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.EUR, Currency.USD));
        Assertions.assertEquals(new BigDecimal("846.13"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.EUR, Currency.RUB));
        Assertions.assertEquals(new BigDecimal("103.60"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.EUR, Currency.SEK));

        Assertions.assertEquals(new BigDecimal("0.12"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.RUB, Currency.EUR));
        Assertions.assertEquals(new BigDecimal("10.1"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.RUB, Currency.RUB));
        Assertions.assertEquals(new BigDecimal("0.14"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.RUB, Currency.USD));
        Assertions.assertEquals(new BigDecimal("1.24"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.RUB, Currency.SEK));

        Assertions.assertEquals(new BigDecimal("8.97"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.USD, Currency.EUR));
        Assertions.assertEquals(new BigDecimal("751.72"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.USD, Currency.RUB));
        Assertions.assertEquals(new BigDecimal("10.1"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.USD, Currency.USD));
        Assertions.assertEquals(new BigDecimal("92.04"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.USD, Currency.SEK));

        Assertions.assertEquals(new BigDecimal("0.98"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.SEK, Currency.EUR));
        Assertions.assertEquals(new BigDecimal("82.49"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.SEK, Currency.RUB));
        Assertions.assertEquals(new BigDecimal("1.11"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.SEK, Currency.USD));
        Assertions.assertEquals(new BigDecimal("10.1"), Conversions.convertToCurrency(new BigDecimal("10.1"), Currency.SEK, Currency.SEK));

    }

    @Test
    void convertStringToDecimal() {
        Assertions.assertNull(Conversions.convertStringToDecimal(null));
        Assertions.assertNull(Conversions.convertStringToDecimal(""));
        Assertions.assertNull(Conversions.convertStringToDecimal(" "));
        Assertions.assertNull(Conversions.convertStringToDecimal("12a"));
        Assertions.assertNull(Conversions.convertStringToDecimal("1,1"));
        Assertions.assertEquals(new BigDecimal("11.56"), Conversions.convertStringToDecimal("11.555"));
        Assertions.assertEquals(new BigDecimal("11.56"), Conversions.convertStringToDecimal("11.556"));
        Assertions.assertEquals(new BigDecimal("11.55"), Conversions.convertStringToDecimal("11.554"));
    }
}
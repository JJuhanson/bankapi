package com.exercise.bankapi.api;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
class AccountControllerTest {

    @Autowired
    private MockMvc mvc;

    private ResultActions getUserAccounts(String user) throws Exception {
        return mvc.perform(get("/accounts/" + user)
                .contentType(MediaType.APPLICATION_JSON));
    }

    private ResultActions addMoneyToUser(String user, String currency, String amount) throws Exception {
        return makePostCall(String.format("/accounts/%s/add", user), String.format("{\"currency\": \"%s\", \"amount\": \"%s\"}", currency, amount));
    }

    private ResultActions debitMoneyFromUser(String user, String currency, String amount) throws Exception {
        return makePostCall(String.format("/accounts/%s/debit", user), String.format("{\"currency\": \"%s\", \"amount\": \"%s\"}", currency, amount));
    }

    private ResultActions exchangeMoneyForUser(String user, String fromCurrency, String toCurrency, String amount) throws Exception {
        return makePostCall(String.format("/accounts/%s/exchange", user), String.format("{\"fromCurrency\": \"%s\",\"toCurrency\": \"%s\",\"amount\": \"%s\"}", fromCurrency, toCurrency, amount));
    }

    private ResultActions makePostCall(String path, String json) throws Exception {
        return mvc.perform(post(path)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json));
    }

    @Test
    public void getAccounts() throws Exception {
        getUserAccounts("admin")
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].amount").value(0.05))
                .andExpect(jsonPath("$[0].currency").value("USD"));
    }

    @Test
    public void addMoney() throws Exception {
        String user = "integration_test_user_add";
        addMoneyToUser(user, "sek", "121.79")
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.amount").value(121.79))
                .andExpect(jsonPath("$.currency").value("SEK"));
    }
    @Test
    public void addIncorrectAmount() throws Exception {
        String user = "integration_test_user_incorrect_add";
        addMoneyToUser(user, "sek", "-10")
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("Provided amount is not correct"));
    }

    @Test
    public void debit() throws Exception {
        String user = "integration_test_user_debit";
        addMoneyToUser(user, "usd", "3555.41").andReturn();

        debitMoneyFromUser(user, "usd", "1000.19")
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.amount").value(2555.22))
                .andExpect(jsonPath("$.currency").value("USD"));
    }

    @Test
    public void debitNotSufficient() throws Exception {
        String user = "integration_test_user_debit_not_sufficient";
        addMoneyToUser(user, "usd", "111").andReturn();

        debitMoneyFromUser(user, "usd", "112")
                .andExpect(status().is(HttpStatus.BAD_REQUEST.value()))
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.message").value("User has insufficient funds"));
    }

    @Test
    public void exchange() throws Exception {
        String user = "integration_test_user_exchange";
        addMoneyToUser(user, "eur", "1000.01").andReturn();
        exchangeMoneyForUser(user, "eur", "rub", "900")
                .andExpect(status().isOk())
                .andExpect(content()
                        .contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.[?(@.currency == 'EUR' && @.amount == 100.01)]").exists())
                .andExpect(jsonPath("$.[?(@.currency == 'RUB' && @.amount == 75397.95)]").exists());
    }
}
# BankAPI

This is Swedbank interview task 


## Task
Create a REST api for bank account handling in Java

- add money to account
- debit money from account
- get account balance
- currency exchange (for simplicity with fixed prices for different currencies)

Requirements:
- account can have multiple currencies (eur, usd, sek, rub) with separate balance
- debiting order can only use one currency (no automatic currency exchange)
- before debiting, simulate a call to external system (ie. external logging) by calling some web page (like https://httpstat.us/)

- microservice (self contained)
- saved in db (pick your own, h2 is ok)
- write defensive code
- unit tests!
- at least a few integration tests are a plus
- remember that money is at stake, use correct types
- api documentation is a plus
- keep in mind that every nanosecond counts for the end user

## Useful links

- http://localhost:8080/h2-console
- http://localhost:8080/swagger-ui.html

